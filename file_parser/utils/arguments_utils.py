import argparse
import os


# argparse types
def positive_int(arg):
    try:
        result = int(arg)
    except ValueError:
        raise argparse.ArgumentTypeError(f'invalid int value: {arg}')
    if result < 1:
        raise argparse.ArgumentTypeError(f'value has to be greater than 0')
    return result


def file(path):
    if os.path.exists(path):
        return path
    else:
        raise argparse.ArgumentTypeError(f'File not found')


# parameters utils
def get_parameters_from_file(path_to_file):
    print('Calculating File Mode')
    spaces_count = 0
    tabs_count = 0
    with open(path_to_file) as file:
        for line in file.readlines():
            if line[0] == ' ':
                spaces_count += 1
            elif line[0] == '\t':
                tabs_count += 1
    if spaces_count > tabs_count:
        print('Guessing that file is using spaces')
        return 'spaces'
    else:
        print('Guessing that file is using tabs')
        return 'tabs'


def get_parameters():
    arg_parser = argparse.ArgumentParser(description='Text-Parser')
    tab_chars = 4
    replace = False

    arg_parser.add_argument('path_to_file', type=file, help='path to file')
    arg_parser.add_argument('-f', '--from', type=str, choices=['tabs', 'spaces'],
                            help='Character to parse from, possible values: "tabs", "spaces"')
    arg_parser.add_argument('-r', '--replace', action='store_true', help='Replace your file WITHOUT backup')
    arg_parser.add_argument('-t', '--tab-chars', type=positive_int, help='How many spaces equals tab',
                            default=4)
    args = arg_parser.parse_args().__dict__
    path_to_file = args['path_to_file']
    if args['from'] is None:
        parse_mode = get_parameters_from_file(path_to_file)
    else:
        tab_chars = args['tab_chars']
        parse_mode = args['from']
        replace = args['replace']
    print(f'Parser parameters: Mode:{parse_mode}, Replace:{replace}, Tab-chars:{tab_chars}')
    return path_to_file, parse_mode, tab_chars, replace
