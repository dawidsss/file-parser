from file_parser import parser as file_parser
from file_parser.utils.arguments_utils import get_parameters

if __name__ == "__main__":
    parser = file_parser.Parser(*get_parameters())
    parser.parse()
