import os
import shutil
from datetime import datetime


class Parser:
    BACKUP_PATH = 'file_parser/backups/'
    parse_mode = None
    parse_from = None
    parse_to = None
    tab_chars = 4
    path_to_file = None
    replaced_lines = 0

    def __init__(self, path_to_file, parse_mode, tab_chars, replace):
        self.path_to_file = path_to_file
        self.parse_mode = parse_mode
        self.tab_chars = tab_chars
        self.replace = replace

        if parse_mode == 'tabs':
            self.parse_from = '\t'
            self.parse_to = ' ' * tab_chars
        else:
            self.parse_from = ' ' * tab_chars
            self.parse_to = '\t'

    def create_backup(self):
        if not os.path.exists(self.BACKUP_PATH):
            os.mkdir(self.BACKUP_PATH)
        name = os.path.splitext(os.path.basename(self.path_to_file))
        time = datetime.now().strftime('%y%m%d-%H%M%S')
        dest_path = f'{self.BACKUP_PATH}{name[0]}-{time}{name[1]}'
        shutil.copyfile(self.path_to_file, dest_path)

    def replace_line(self, line):
        not_whitespace_index = 0
        for char in line:
            if char == self.parse_from[0]:
                not_whitespace_index += 1
            else:
                break
        if line.startswith(self.parse_from):
            self.replaced_lines += 1
        return line[:not_whitespace_index].replace(self.parse_from, self.parse_to) + line[not_whitespace_index:]

    def _parse(self):
        result = list()
        with open(self.path_to_file) as file:
            for line in file.readlines():
                result.append(self.replace_line(line))
        return result

    def parse(self):
        if not self.replace:
            self.create_backup()
        result = self._parse()
        with open(self.path_to_file, 'w+') as file:
            file.writelines(result)
        print(f'Parsed successful! replaced lines: {self.replaced_lines}')
        return result
