import unittest
from unittest import mock

from file_parser import parser


class TestParserMethodReplaceLine(unittest.TestCase):
    def test_replace_line_spaces_mode_one_tab_successful(self):
        file_parser = parser.Parser('path_to_file', 'spaces', 4, True)

        replaced_line = file_parser.replace_line('    Some Text\tMore Text ')

        self.assertEqual(replaced_line, '\tSome Text\tMore Text ')

    def test_replace_line_spaces_mode_more_tabs_successful(self):
        file_parser = parser.Parser('path_to_file', 'spaces', 4, True)

        replaced_line = file_parser.replace_line('         Some Text\tMore Text ')

        self.assertEqual(replaced_line, '\t\t Some Text\tMore Text ')

    def test_replace_line_spaces_greater_tab_chars_successful(self):
        file_parser = parser.Parser('path_to_file', 'spaces', 7, True)

        replaced_line = file_parser.replace_line('       Some Text\tMore Text ')

        self.assertEqual(replaced_line, '\tSome Text\tMore Text ')

    def test_replace_line_spaces_without_changes_successful(self):
        file_parser = parser.Parser('path_to_file', 'spaces', 4, True)
        line = '  Some Text\tMore Text '

        replaced_line = file_parser.replace_line(line)

        self.assertEqual(replaced_line, line)

    def test_replace_line_tabs_mode_one_tab_successful(self):
        file_parser = parser.Parser('path_to_file', 'tabs', 4, True)

        replaced_line = file_parser.replace_line('\tSome Text\tMore Text ')

        self.assertEqual(replaced_line, '    Some Text\tMore Text ')

    def test_replace_line_tabs_mode_more_tabs_successful(self):
        file_parser = parser.Parser('path_to_file', 'tabs', 4, True)

        replaced_line = file_parser.replace_line('\t\t Some Text\tMore Text ')

        self.assertEqual(replaced_line, '         Some Text\tMore Text ')

    def test_replace_line_spaces_mode_add_to_lines_successful(self):
        file_parser = parser.Parser('path_to_file', 'spaces', 4, True)

        file_parser.replace_line('        Some Text\tMore Text ')

        self.assertEqual(file_parser.replaced_lines, 1)

    def test_replace_line_spaces_mode_dont_add_to_lines_successful(self):
        file_parser = parser.Parser('path_to_file', 'spaces', 4, True)

        file_parser.replace_line(' Some Text\tMore Text ')

        self.assertEqual(file_parser.replaced_lines, 0)

    def test_replace_line_tabs_mode_add_to_lines_successful(self):
        file_parser = parser.Parser('path_to_file', 'tabs', 4, True)

        file_parser.replace_line('\tSome Text\tMore Text ')

        self.assertEqual(file_parser.replaced_lines, 1)


class TestParserMethodParse(unittest.TestCase):
    spaces_data = [
        "    This is test data    \n",
        "This is    test data\n",
        "         This is test\tdata\t"
    ]
    tabs_data = [
        "\tThis is test data    \n",
        "This is    test data\n",
        "\t\t This is test\tdata\t"
    ]

    def test_parse_spaces_mode_successful(self):
        file_parser = parser.Parser('path_to_file', 'spaces', 4, True)
        mo = mock.mock_open()
        handlers = (mock.mock_open(read_data=str().join(self.spaces_data)).return_value,)
        mo.side_effect = handlers

        with mock.patch('builtins.open', mo):
            parsed_text = file_parser._parse()
        self.assertEqual(parsed_text, self.tabs_data)

    def test_parse_tabs_mode_successful(self):
        file_parser = parser.Parser('path_to_file', 'tabs', 4, True)
        mo = mock.mock_open()
        handlers = (mock.mock_open(read_data=str().join(self.tabs_data)).return_value,)
        mo.side_effect = handlers

        with mock.patch('builtins.open', mo):
            parsed_text = file_parser._parse()

        self.assertEqual(parsed_text, self.spaces_data)

    def test_parse_spaces_mode_replaced_lines_count(self):
        file_parser = parser.Parser('path_to_file', 'spaces', 4, True)
        mo = mock.mock_open()
        handlers = (mock.mock_open(read_data=str().join(self.spaces_data)).return_value,)
        mo.side_effect = handlers

        with mock.patch('builtins.open', mo):
            file_parser._parse()
        self.assertEqual(file_parser.replaced_lines, 2)

    def test_parse_tabs_mode_replaced_lines_count(self):
        file_parser = parser.Parser('path_to_file', 'tabs', 4, True)
        mo = mock.mock_open()
        handlers = (mock.mock_open(read_data=str().join(self.tabs_data)).return_value,)
        mo.side_effect = handlers

        with mock.patch('builtins.open', mo):
            file_parser._parse()
        self.assertEqual(file_parser.replaced_lines, 2)
