import sys
import unittest
from unittest import mock

from file_parser.utils import arguments_utils


class TestGetParameters(unittest.TestCase):
    def setUp(self):
        arguments_utils.file = mock.MagicMock(return_value='path_to_file')

    def test_get_parameters_successful(self):
        sys.argv = ['path_to_module', 'path_to_file', '-f', 'spaces', '-r', '-t', '5']

        args = arguments_utils.get_parameters()

        self.assertEqual(args, ('path_to_file', 'spaces', 5, True))

    def test_get_parameters_without_replace_successful(self):
        sys.argv = ['path_to_module', 'path_to_file', '-f', 'spaces', '-t', '5']

        args = arguments_utils.get_parameters()

        self.assertEqual(args, ('path_to_file', 'spaces', 5, False))

    def test_get_parameters_without_tab_chars_successful(self):
        sys.argv = ['path_to_module', 'path_to_file', '-f', 'spaces', '-r']

        args = arguments_utils.get_parameters()

        self.assertEqual(args, ('path_to_file', 'spaces', 4, True))

    def test_get_parameters_without_replace_and_tab_chars_successful(self):
        sys.argv = ['path_to_module', 'path_to_file', '-f', 'spaces']

        args = arguments_utils.get_parameters()

        self.assertEqual(args, ('path_to_file', 'spaces', 4, False))

    def test_get_parameters_with_tabs_successful(self):
        sys.argv = ['path_to_module', 'path_to_file', '-f', 'tabs']

        args = arguments_utils.get_parameters()

        self.assertEqual(args, ('path_to_file', 'tabs', 4, False))

    def test_get_parameters_wrong_from_parameter(self):
        sys.argv = ['path_to_module', 'path_to_file', '-f', 'wrong_parameter']

        with self.assertRaises(SystemExit):
            arguments_utils.get_parameters()

    def test_get_parameters_wrong_tab_chars_parameter(self):
        sys.argv = ['path_to_module', 'path_to_file', '-f', 'tabs', '-t', 'not_int']

        with self.assertRaises(SystemExit):
            arguments_utils.get_parameters()


class TestGetParametersFromFile(unittest.TestCase):
    spaces_data = (
        "    This is test data    \n"
        "\tThis is    test data\n"
        "         This is test\tdata\t"
    )
    tabs_data = (
        "\tThis is test data    \n"
        "    This is    test data\n"
        "\t\t This is test\tdata\t"
    )

    def test_get_parameters_from_file_spaces(self):
        mo = mock.mock_open()
        handlers = (mock.mock_open(read_data=self.spaces_data).return_value,)
        mo.side_effect = handlers

        with mock.patch('builtins.open', mo):
            result = arguments_utils.get_parameters_from_file('path_to_file')

        self.assertEqual(result, 'spaces')

    def test_get_parameters_from_file_tabs(self):
        mo = mock.mock_open()
        handlers = (mock.mock_open(read_data=self.tabs_data).return_value,)
        mo.side_effect = handlers

        with mock.patch('builtins.open', mo):
            result = arguments_utils.get_parameters_from_file('path_to_file')

        self.assertEqual(result, 'tabs')
